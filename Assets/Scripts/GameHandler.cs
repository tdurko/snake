﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    private LevelGrid level;
    [SerializeField] private Snake snake;
    private static GameHandler instance;
    private static int score;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Debug.Log("GameHandler.Start");
 
        level = new LevelGrid(20, 20);
        snake.Setup(level);
        level.Setup(snake);

    }
    public static void ResetScore()
    {
        score= 0;
    }

    public static int GetScore()
    {
        return score;
    }

    public static void AddScore()
    {
        score += 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
