﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverButton : MonoBehaviour
{
    private Button gameOverButton;
    private static GameOverButton instance;
    private void Awake()
    {
        instance = this;
        gameOverButton = transform.Find("GameOverButton").GetComponent<Button>();
        gameOverButton.onClick.AddListener(TaskOnClick);
        Hide();

    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    void TaskOnClick()
    {
        Loader.Load(Loader.Scene.SampleScene);

    }

    public static void ShowStatic()
    {
        instance.Show();
    }
    private void Update()
    {


    }
}
