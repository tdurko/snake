﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid
{
    private Vector2Int foodGridPostition;
    private int width;
    private int height;
    private GameObject foodGameObject;
    private Snake snake;
    private bool start = true;
    public void Setup(Snake snake)
    {
        this.snake = snake;
    }

    public LevelGrid(int width, int height)
    {
        this.width = width;
        this.height = height;
        SpawnFood();
    }

    private void SpawnFood()
    {
        foodGridPostition = new Vector2Int(Random.Range(0, width), Random.Range(0, height));
        if(start)
        {
            foodGameObject = new GameObject("Food", typeof(SpriteRenderer));
            foodGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.foodSprite;
            foodGameObject.transform.position = new Vector3(foodGridPostition.x, foodGridPostition.y);
            start = false;
        }
        else
        {
            if (FreeToSpawn(foodGridPostition))
            {
                foodGameObject = new GameObject("Food", typeof(SpriteRenderer));
                foodGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.foodSprite;
                foodGameObject.transform.position = new Vector3(foodGridPostition.x, foodGridPostition.y);
            }
            else
                SpawnFood();

        }

    }

    private bool FreeToSpawn(Vector2Int grid)
    {
        
        foreach(Vector2Int vector in snake.GetSnakePositions())
        {
            if (vector == grid)
            {
                return false;
            }
        }
        return true;
    }

    public bool SnakeEats(Vector2Int snakeGridPosition)
    {
        if (snakeGridPosition == foodGridPostition)
        {
            Object.Destroy(foodGameObject);
            SpawnFood();
            return true;
        }
        else
            return false;
            
    }
    public Vector2Int ValidatePosition(Vector2Int position)
    {
        if(position.x<0)
        {
            position.x = width;
        }
        if(position.x>width)
        {
            position.x = 0;
        }
        if(position.y<0)
        {
            position.y = height;
        }
        if(position.y>height)
        {
            position.y = 0;
        }

        return position;
    }
}
