﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
   private enum State
    {
        Alive,
        Death
    }
    State state;
    private Vector2Int gridPosition;
    private float gridTimerMax;
    private float gridTimer;
    private Vector2Int gridDirection;
    private LevelGrid levelGrid;
    private int snakeSize;
    private List<GameObject> bodyGameObjects;
    private Vector2Int bodyPosition;

    public void Setup(LevelGrid levelGrid)
    {
        this.levelGrid = levelGrid;
    }

    private void Awake()
    {
        gridPosition = new Vector2Int(10,10);
        bodyPosition = new Vector2Int(10, 10);
        gridTimerMax = 0.3f;
        gridTimer = gridTimerMax;
        gridDirection = new Vector2Int(0, 1);
        snakeSize = -1;
        bodyGameObjects = new List<GameObject>();
        state = State.Alive;

    }

    public bool FreeToSpawn(Vector2Int vector)
    {
        foreach (GameObject bodyPart in bodyGameObjects)
        {
            if (bodyPart.transform.position.x == vector.x && bodyPart.transform.position.y == vector.y)
            {
                return false;
            }
        }
        if (transform.position.x == vector.x && transform.position.y == vector.y)
            return false;
        return true;
    }

    private float GetAngleFromVector(Vector2Int dir)
    {
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        n -= 90;
        if (n < 0)
            n += 360;
        return n;
    }

    private void AddBody(Vector2Int position, int index)
    {
        GameObject body;
        body = new GameObject("Body", typeof(SpriteRenderer));
        body.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.bodySprite;
        body.transform.position = new Vector3(position.x-gridDirection.x, position.y-gridDirection.y);
        bodyGameObjects.Insert(index, body);
    }

    private void InputHandler()
    {
        if (Input.GetKeyDown(KeyCode.D) && gridDirection.x != -1)
        {
            gridDirection.x = 1;
            gridDirection.y = 0;
        }
        if (Input.GetKeyDown(KeyCode.W) && gridDirection.y != -1)
        {
            gridDirection.x = 0;
            gridDirection.y = 1;
        }
        if (Input.GetKeyDown(KeyCode.A) && gridDirection.x != 1)
        {
            gridDirection.x = -1;
            gridDirection.y = 0;
        }
        if (Input.GetKeyDown(KeyCode.S) && gridDirection.y != 1)
        {
            gridDirection.x = 0;
            gridDirection.y = -1;
        }
    }

    public List<Vector2Int> GetSnakePositions()
    {
        List<Vector2Int> Lista = new List<Vector2Int>();
        Lista.Add(new Vector2Int((int)transform.position.x, (int)transform.position.y));
        foreach(GameObject gameObject in bodyGameObjects)
        {
            Lista.Add(new Vector2Int((int)gameObject.transform.position.x, (int)gameObject.transform.position.y));
        }
        return Lista;
    }

    private void MovmentHandler()
    {
        gridTimer += Time.deltaTime;
        if (gridTimer >= gridTimerMax)
        {
            for (int i = snakeSize; i > 0; i--)
            {
                bodyGameObjects[i].transform.position = bodyGameObjects[i-1].transform.position;
            }
            if(snakeSize!=-1)
                bodyGameObjects[0].transform.position = new Vector3(gridPosition.x, gridPosition.y);
            gridPosition.x += gridDirection.x;
            gridPosition.y += gridDirection.y;
            gridTimer -= gridTimerMax;
            gridPosition = levelGrid.ValidatePosition(gridPosition);
            transform.position = new Vector3(gridPosition.x, gridPosition.y);
            transform.eulerAngles = new Vector3(0, 0, GetAngleFromVector(gridDirection));
            if(levelGrid.SnakeEats(gridPosition))
            {
                snakeSize++;
                AddBody(gridPosition, snakeSize);
                GameHandler.AddScore(); 
                
            }
            if(SnakeHitBody(gridPosition))
            {
                // Game Over
                Debug.Log("You Lost");
                state = State.Death;
                GameOverButton.ShowStatic();
            }
            
        }
    }
    private bool SnakeHitBody(Vector2Int glowa)
    {
        foreach(GameObject bodyPart in bodyGameObjects)
        {
            if (bodyPart.transform.position == transform.position)
            {
                return true;
            }
        }
        return false;
    }

    private void Update()
    {   if(state == State.Alive)
        {
            InputHandler();
            MovmentHandler();
        }

    }
}
