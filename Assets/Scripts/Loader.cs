﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Loader
{
    public enum Scene
    {
        SampleScene,
    }

    public static void Load(Scene scene)
    {
        GameHandler.ResetScore();
        SceneManager.LoadScene(scene.ToString());

    }
}
